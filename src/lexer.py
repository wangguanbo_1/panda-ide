import re

def lex(text):
    text = text.lstrip(' ').lstrip('\t').lstrip('n')
    if text=='':
        return []
    tokens = []
    lastToken = ['Skip','']
    for i in text:
        if lastToken[0]=='Skip':
            if re.search('[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOQRSTUVWXYZ_]',i):
                tokens.append(['Ident',i])
                lastToken = ['Ident',i]
            elif re.search('[\(\)\+\-\*\/\%\^\>\<\=\,\:\;]',i):
                tokens.append(['Opr',i])
                lastToken = ['Opr',i]
            elif i=='#':
                tokens.append(['Text',i])
                lastToken = ['Text',i]
            elif re.search('[0123456789.]',i):
                tokens.append(['Num',i])
                lastToken = ['Num',i]
            elif i==' ' or i=='\t' or i=='\n' and lastToken[1]!='':
                tokens[len(tokens)-1] = ['Skip',lastToken[1]+i]
                lastToken = ['Skip',tokens[len(tokens)-1][1]]
            elif i=='"':
                tokens.append(['String',i])
                lastToken = ['String',i]
            else:
                tokens.append(['Skip',i])
                lastToken = ['Skip',i]
        else:
            if lastToken[0]=='String':
                if i=='"':
                    tokens[len(tokens)-1] = ['CString',lastToken[1]+i]
                    lastToken = ['CString',tokens[len(tokens)-1][1]]
                else:
                    tokens[len(tokens)-1] = ['String',lastToken[1]+i]
                    lastToken = ['String',tokens[len(tokens)-1][1]]
            elif i==' ' or i=='\t' or i=='\n' or i=='\r':
                tokens.append(['Skip',i])
                lastToken = ['Skip',i]
            elif lastToken[0]=='Text':
                tokens[len(tokens)-1] = ['Text',lastToken[1]+i]
                lastToken = ['Text',tokens[len(tokens)-1][1]]
            elif lastToken[0]=='Opr' and i=='=':
                tokens[len(tokens)-1] = ['Opr',lastToken[1]+i]
                lastToken = ['Opr',tokens[len(tokens)-1][1]]
            elif lastToken[0]=='Num' and re.search('[0123456789.]',i):
                tokens[len(tokens)-1] = ['Num',lastToken[1]+i]
                lastToken = ['Num',tokens[len(tokens)-1][1]]
            elif lastToken[0]=='Ident' and re.search('[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOQRSTUVWXYZ_0123456789]',i):
                tokens[len(tokens)-1] = ['Ident',lastToken[1]+i]
                lastToken = ['Ident',tokens[len(tokens)-1][1]]
                if lastToken[1]=='while' or lastToken[1]=='for' or lastToken[1]=='if' or lastToken[1]=='else' or lastToken[1]=='var' or lastToken[1]=='and' or lastToken[1]=='or' or lastToken[1]=='not':
                    tokens[len(tokens)-1] = ['Key',lastToken[1]]
            else:
                if re.search('[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOQRSTUVWXYZ_]',i):
                    tokens.append(['Ident',i])
                    lastToken = ['Ident',i]
                elif re.search('[\(\)\+\-\*\/\%\^\>\<\=\,\:\;]',i):
                    tokens.append(['Opr',i])
                    lastToken = ['Opr',i]
                elif i=='#':
                    tokens.append(['Text',i])
                    lastToken = ['Text',i]
                elif re.search('[0123456789.]',i):
                    tokens.append(['Num',i])
                    lastToken = ['Num',i]
                elif i==' ' or i=='\t' or i=='\n' or i=='\r':
                    tokens[len(tokens)-1] = ['Skip',lastToken[1]+i]
                    lastToken = ['Skip',tokens[len(tokens)-1][1]]
                elif i=='"':
                    tokens.append(['String',i])
                    lastToken = ['String',i]
    return tokens