# Panda-IDE —— 一个基于Tkinter的Panda语言IDE
![](https://img.shields.io/badge/Language-Python-blue) ![](https://img.shields.io/static/v1?label=Platforms&message=Windows|Linux&color=orange)  [![](https://img.shields.io/static/v1?label=Release&message=v1.0.0-pre1&color=red)](https://gitee.com/wangguanbo_1/panda-ide/releases/v1.0.0-pre1)  
---
此项目是Panda官方推出的IDE，您可以使用最新的发行版。当然，您也可以尝试自行运行、打包和对此仓库进行贡献。  
此项目支持：  
正常编辑  
语法高亮  
自动补全和自动缩进(todo)  
## 运行
如果您想从.py文件运行此项目，请在命令行内输入：  
```shell
git clone https://gitee.com/wangguanbo_1/panda-ide
cd panda-ide
python main.py
```
## 打包成可执行文件
如果您想将此仓库的.py文件打包成可执行文件，请先安装pyinstaller：
```shell
python -m pip install pyinstaller
```
然后输入如下命令：
```shell
pyinstaller -F -W main.py
```
打包的可执行文件为dist/main(.exe)  
## 对此仓库进行贡献
如果发现BUG，您可以提交issue。当然，您需要先检查有没有人已经提交过issue。  
如果您想自己修复BUG，可以发起轻量级Pull Requests，当然也可以Fork此仓库，修改代码后发起Pull Requests。