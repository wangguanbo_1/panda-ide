import tkinter as tk
import os,sys
import tkinter.filedialog as tkfd
import lexer
import threading
import time

def doOpen():
    global text
    file_name = tkfd.askopenfilename(title='选择文件',initialdir=sys.path[0],filetypes=[("Panda File", ".pd")])
    with open(file_name,'r',encoding='utf-8') as f:
        file_data = f.read()
    text.delete(1.0,tk.END)
    text.insert(0.0,file_data)

def doSave():
    global text
    file_name = tkfd.asksaveasfilename(title='保存文件',initialdir=sys.path[0],filetypes=[("Panda File", ".pd")])
    with open(file_name+'.pd','w',encoding='utf-8') as f:
        f.write(text.get(1.0,tk.END))

def run():
    pass

window = tk.Tk()
window.title('Panda IDE')
text = tk.Text(window)
text.tag_config("Red_White",background="White",foreground="Red")
text.tag_config("Blue_White",background="White",foreground="Blue")
text.tag_config("Black_White",background="White",foreground="Black")
text.tag_config("Green_White",background="White",foreground="Green")
text.tag_config("Red_LightCyan",background="LightCyan",foreground="Red")
text.tag_config("Blue_LightCyan",background="LightCyan",foreground="Blue")
text.tag_config("Black_LightCyan",background="LightCyan",foreground="Black")
text.tag_config("Green_LightCyan",background="LightCyan",foreground="Green")
text.grid(row=0,column=0)
menuBar = tk.Menu(window)
editmenu = tk.Menu(menuBar,tearoff=0)
menuBar.add_cascade(label='文件',menu=editmenu)
editmenu.add_command(label='打开', command=doOpen)
editmenu.add_command(label='保存', command=doSave)
menuBar.add_command(label='运行', command=run)
window.config(menu=menuBar)

def toIndex(x):
    global text
    lines=1
    index=0
    for i in range(0,x):
        if text.get(1.0,tk.END)[i]=='\n':
            lines+=1
            index=0
        else:
            index+=1
    return str(lines)+'.'+str(index)

def doHighLight():
    time.sleep(1)
    global text
    last = ''
    while True:
        if last!=text.get(1.0,tk.END)[0:len(text.get(1.0,tk.END))-1]:
            now = text.get(1.0,tk.END)[0:len(text.get(1.0,tk.END))-1]
            lex = lexer.lex(now)
            insertLen = len(text.get(1.0,'insert'))
            text.delete(1.0,tk.END)
            for i in lex:
                tag = 'Black_'
                if i[0]=='Opr':
                    tag = 'Red_'
                elif i[0]=='Key' or i[0]=='CString' or i[0]=='String':
                    tag = 'Blue_'
                elif i[0]=='Text':
                    tag = 'Green_'
                tag+='White'
                text.insert(tk.END,i[1],tag)
            last = text.get(1.0,tk.END)[0:len(text.get(1.0,tk.END))-1]
            text.mark_set('insert',toIndex(insertLen))

highLight = threading.Thread(target=doHighLight,daemon=True)
highLight.start()
window.mainloop()